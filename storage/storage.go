// Package storage contains the code to persist and retrieve data from
// Yugabyte
package storage

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/gocql/gocql"
	"github.com/levenlabs/go-llog"
)

type Instance struct {
	database string
	host     string
	port     int
	user     string
	password string

	DB *gocql.Session
}

func New(dopplerConf *DopplerConfig) *Instance {
    cluster := 
	inst := &Instance{}

	if overrideDatabase != "" {
		inst.database = overrideDatabase
	} else {
		inst.database = "battlechats_admin"
	}

	// Ideally we will get these from Doppler
	inst.host = "localhost"
	inst.port = 5432
	inst.user = "yugabyte"
	inst.password = "overlysecurepassword"

	psqlconn := fmt.Sprintf("host=%s post=%s user=%s password=%s dbname=%s",
		inst.host, inst.port, inst.user, inst.password, inst.database)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		llog.Fatal("failed to connect to database")
	}

	inst.DB = db
	return inst
}

func (i *Instance) ensureSchema(ctx context.Context) error {

	g := `CREATE TABLE IF NOT EXISTS games (
    id TEXT NOT NULL,
    name TEXT NOT NULL,
    create_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id)
)`

	_, err := i.DB.ExecContext(ctx, g)
	if err != nil {
		return err
	}

	u := `CREATE TABLE IF NOT EXISTS users (
        id TEXT NOT NULL,
        email TEXT NOT NULL, 
        oauth_token TEXT NOT NULL
        PRIMARY KEY(id)
)`

	_, err = i.DB.ExecContext(ctx, u)
	if err != nil {
		return err
	}

	ug := `CREATE TABLE IF NOT EXISTS user_games (
        game_id TEXT NOT NULL,
        user_id TEXT NOT NULL,
        score INT NOT NULL DEFAULT 0,
        CONSTRAINT fk_game_id(game_id) REFERENCES games(id) 
        CONSTRAINT fk_user_id(user_id) REFERENCES users(id) 
    )`

	_, err = i.DB.ExecContext(ctx, ug)
	if err != nil {
		return err
	}

	return nil
}

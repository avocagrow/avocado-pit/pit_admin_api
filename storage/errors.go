package storage

import "errors"

var (
	ErrNotFound = errors.New("DB record not found")
)

package storage

import (
	"context"

	"gitlab.com/avocagrow/avocado-pit/pit_admin_api/api"
)

func (i *Instance) CreateGame(ctx context.Context, g api.Game) (string, error) {
	return "", api.ErrNotImplemented
}

func (i *Instance) GetGame(ctx context.Context, id string) (api.Game, error) {
	return api.Game{}, api.ErrNotImplemented
}

func (i *Instance) GetGames(ctx context.Context) ([]api.Game, error) {
	return nil, api.ErrNotImplemented
}

func (i *Instance) UpdateGame(ctx context.Context, g api.Game) error {
	return api.ErrNotImplemented
}

func (i *Instance) DeleteGame(ctx context.Context, id string) error {
	return api.ErrNotImplemented
}

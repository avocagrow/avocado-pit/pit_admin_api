package storage

import (
	"context"
	"errors"
	"testing"

	"github.com/levenlabs/go-llog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/avocagrow/avocado-pit/pit_admin_api/api"
)

func TestGetGame(t *testing.T) {
	// create a context to use for our tests
	ctx := context.Background()

	inst := New(randomString("test_", "_get_game"))
	game := api.Game{
		Id:   randomString("", ""),
		Name: randomString("", ""),
	}

	id, err := inst.CreateGame(ctx, game)
	require.NoError(t, err, llog.ErrWithKV(err))

	got, err := inst.GetGame(ctx, id)
	require.NoError(t, err, llog.ErrWithKV(err))
	assert.Equal(t, game.Id, got.Id)
	assert.Equal(t, game.Name, got.Name)

	// check for one that doesn't exist
	got, err = inst.GetGame(ctx, "notfound")
	require.NoError(t, err, llog.ErrWithKV(err))

	if assert.Error(t, err) {
		assert.True(t, errors.Is(err, ErrNotFound), "%#v", err)
	}

	err = inst.DeleteGame(ctx, id)
	require.NoError(t, err, llog.ErrWithKV(err))

}

func TestGetGames(t *testing.T) {
}

func TestCreateGame(t *testing.T) {
}

func TestUpdateGame(t *testing.T) {
}

func TestDeleteGame(t *testing.T) {
}

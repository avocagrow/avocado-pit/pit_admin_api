package storage

import (
	"context"
	"crypto/rand"
	"fmt"
)

func randomString(prefix string, suffix string) string {
	b := make([]byte, 12)
	_, err := rand.Read(b)
	if err != nil {
		// this should never happen, and if it does, then we forgot to turn the airfryer off
		panic(err)
	}
	return fmt.Sprintf("%s%x%s", prefix, b, suffix)
}

func (i *Instance) removeDB(ctx context.Context, dbName string) error {
	_, err := i.DB.ExecContext(ctx, `DROP DATABASE $1`, dbName)
	if err != nil {
		return err
	}
	return nil
}

package config

import (
	"log"
	"os"
)

var (
	dopplerEnvToken = "DOPP_PIT_ADM_TOKEN"
)

type Config struct {
	ycql *ycqlConfig
}

type ycqlConfig struct {
	dc       string
	host     string
	port     int
	rootCert string
}

// creates a new instance of DopplerConfig which retrieves the
// config for the specified project
func New() *Config {
	dopplerToken := os.Getenv(dopplerEnvToken)
	if dopplerToken == "" {
		log.Fatalf("Unable to find %s in env variables", dopplerEnvToken)
	}
	return &Config{}
}

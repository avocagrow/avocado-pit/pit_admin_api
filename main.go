package main

import (
	"flag"
	"net/http"
	"os"
	"os/signal"
)

func main() {
	// flag.String will return a pointer to a string after flag.Parse() is called
	addr := flag.String("listen-addr", "localhost:8888", "the address to listen on for API requests")
	flag.Parse()

	server := new(http.Server)

	server.Addr = *addr

	server.Handler = unimplmenetedHandler

	go server.ListenAndServe()

	defer server.Close()

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt)

	<-ch
}

var unimplmenetedHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "YO NERD, you didn't make this yet.", http.StatusNotImplemented)
})

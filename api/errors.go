package api

import "errors"

var (
	ErrNotImplemented = errors.New("not yet implemented")
)
